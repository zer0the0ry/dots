# zer0the0ry's DotFiles

> Just the dots.

----

## Featuring

* .bashrc
* .vimrc
* .alacritty.yml
* .bashconfig
* .tmux.conf

## Notice

Do not just install and use these. Study them and see what they do. For example vim plug is required for the .vimrc plugins without it, they won't function.  
Use at your own risk.

