########################
# zerotheory's .bashrc #
########################

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# sourcing some files
if [ -f ~/.bashconfig ]; then
    . ~/.bashconfig
fi

# Set vim as editor
export EDITOR='vim'

if [ -f /usr/share/bash-completion/bash_completion ]; then
  . /usr/share/bash-completion/bash_completion
fi

# Don't put duplicate lines or lines starting with space in history.
HISTCONTROL=ignoreboth:erasedups
HISTSIZE=3000
HISTFILESIZE=3000

# set vi mode
set -o vi

## Random PS1 ##

randomprompt=$((RANDOM%7))

#generic PS1
case $randomprompt in 
  "0") PS1='\[\033[1;30m\]-= \w =- \[\033[0;0m\]\n  \[\033[0;0m\]' ;;
  "1") PS1='\[\033[1;30m\][\[\033[1;34m\]\u\[\033[0;0m\]@\[\033[1;33m\]\h\[\033[0;0m\]\[\033[1;30m\]]\n [\[\033[1;31m\]\w\]\[\033[0;31m\]\[\033[1;30m\]]\[\033[1;32m\] \[\033[0;0m\] ' ;;
  "2") PS1='🔹\[\033[0;35m\]\u\[\033[0;0m\]🔻\[\033[1;33m\]\h\[\033[0;0m\]\n \[\033[1;31m\]\w\[\033[0;0m\]🔸\[\033[0;31m\] \[\033[0;0m\] ' ;;
  "3") PS1="\[\033[0;35m\]\342\224\214\342\224\200\$([[ \$? != 0 ]] && echo \"[\[\033[0;31m\]\342\234\227\[\033[0;35m\]]\342\224\200\")[$(if [[ ${EUID} == 0 ]]; then echo '\[\033[0;31m\]\h'; else echo '\[\033[1;34m\]\u\[\033[0;32m\]@\[\033[0;34m\]\h'; fi)\[\033[0;35m\]]\342\224\200(\[\033[0;32m\]\w\[\033[0;35m\])\n\[\033[0;35m\]\342\224\224\342\224\200\342\224\200\[\033[1;31m\] ﰁ๛ \[\033[0m\]" ;;
  "4") PS1='-= \w =-\n 🕱 ';;
  "5") PS1='\[\033[01;30m\] \d \@\n \[\033[01;30m\]user:\[\033[0;36m\] \u\n \[\033[01;30m\]host:\[\033[0;35m\] \h\n \[\033[1;30m\]shell: \[\033[0;32m\]\s\n \[\033[01;30m\]count: \[\033[0;33m\]\#\n \[\033[01;30m\]pwd: \[\033[01;31m\]\w\n \[\033[01;32m\]\$ \[\033[0;00m\]';;
 "6") PS1='\[\033[0;34m\] 𐦉 \w 𐦞 \[\033[0;0m\]\n \[\033[01;35m\] 𐦝  \[\033[0;0m\]';;
esac

## Exercism ## binary is ~/.config/exercism 
if [ -f ~/.config/exercism/exercism_completion.bash  ]; then
      source ~/.config/exercism/exercism_completion.bash
fi

# globstar # 
# use ** to glob recursively in directories. e.g. ls README.md **
shopt -s globstar

## customized start ##
customstart=$((RANDOM%7))

case $customstart in 
  "0") neofetch | lolcat -p .6;;
  "1") me | lolcat;;
  "2") pfetch | lolcat;;
  "3") fortune | lolcat;;
  "4") archey3 | lolcat;;
  "5") fortune | cowsay -f eyes | lolcat;;
  "6") neofetch --ascii_distro BlackArch;;
esac
